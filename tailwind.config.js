module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      zIndex: {
        header: '1000'
      },
      height: {
        header: '8.8rem',
      },
      gridTemplateRows: {
        layout: '1fr'
      },
      spacing: {
        header: '8.8rem',
        layout: '8.8rem 5rem 5rem',
      }
    },
  },
  plugins: [
    require('@tailwindcss/forms')
  ],
}
