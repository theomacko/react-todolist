import Form from "./components/Form";
import Header from "./components/Header";
import Todolist from "./components/Todolist";

function App() {
  return (
    <div className="font-mono bg-gray-900">
      <div className="min-h-screen grid grid-rows-layout">
        <Header />

        <div className="pt-header px-5 md:p-layout flex justify-center">
          <div className="w-full max-w-7xl mt-12 mb-20 flex flex-col">
            <Form />
            <Todolist />
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
