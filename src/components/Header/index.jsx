const Header = () => {
  return (
    <header className="fixed z-header top-0 inset-x-0 h-header py-2 px-4 flex items-center justify-center bg-gray-700">
      <p className="text-6xl text-gray-300 font-bold uppercase">todolist</p>
    </header>
  )
}

export default Header
