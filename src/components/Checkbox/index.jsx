import PropTypes from 'prop-types'

const Checkbox = ({ label, name, checked, method, placeholder = label, id = name }) => {
  return (
    <div className="flex items-center">
      <input className="py-3 px-5 rounded-md text-2xl" type="checkbox" name={name} id={id} placeholder={placeholder} checked={checked} onChange={method} />
      <label className="ml-3" htmlFor={id}>{label}</label>
    </div>
  )
}

Checkbox.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  checked: PropTypes.bool.isRequired,
  method: PropTypes.func.isRequired,
  type: PropTypes.string,
  placeholder: PropTypes.string,
  id: PropTypes.string
}

export default Checkbox
