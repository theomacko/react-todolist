import { motion } from 'framer-motion'
import PropTypes from 'prop-types'
import { useDispatch } from 'react-redux'
import { removeTodo, toggleCompleteTodo } from '../../features/todolist/todolistSlice'

const Todo = ({ data: { id, title, description, completed } }) => {
  // Dispatch
  const dispatch = useDispatch()

  // Methods
  const handleOnToggleCompleted = () => dispatch(toggleCompleteTodo({ id, completed: !completed }))

  const handleOnRemoveTodo = () => dispatch(removeTodo({ id }))

  return (
    <motion.li
      className="relative grid grid-cols-1 gap-3"
      initial={{ x: -100, opacity: 0 }}
      animate={{ x: 0, opacity: 1 }}
      exit={{ x: 100, opacity: 0 }}
      >
        <div className={`pt-5 pr-20 pb-5 pl-5 rounded-md border-4 ${completed ? 'border-red-900 opacity-30' : 'border-emerald-400'} bg-gray-400 duration-300`}>
          <p className="text-3xl font-bold">{title}</p>
          <p>{description}</p>
        </div>
        <button type="button" className="absolute top-2 right-4 z-20 font-extrabold" onClick={handleOnRemoveTodo}>x</button>
        <button type="button" className="absolute inset-0 z-10 opacity-0" onClick={handleOnToggleCompleted}></button>
    </motion.li>
  )
}

Todo.propTypes = {
  data: PropTypes.shape({
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    completed: PropTypes.bool.isRequired,
  })
}

export default Todo
