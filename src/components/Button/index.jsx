import PropTypes from 'prop-types'
import './index.scss'

const Button = ({ title, type = 'button', variant = 'primary', outlined = true, classes = '' }) => {
  return (
    <button
      type={type}
      className={`py-5 px-8 text-3xl font-bold uppercase border-2 rounded-md btn-${variant} ${outlined ? 'btn-' + variant + '--outlined' : ''} ${classes}`}
    >
      { title }
    </button>
  )
}

Button.propTypes = {
  title: PropTypes.string.isRequired,
  type: PropTypes.string,
  variant: PropTypes.string,
  outlined: PropTypes.bool,
  classes: PropTypes.string
}

export default Button
