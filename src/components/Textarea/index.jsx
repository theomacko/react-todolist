import PropTypes from 'prop-types'

const Textarea = ({ label, name, value, method, placeholder = label, id = name }) => {
  return (
    <div className="flex flex-col">
      <label className="mb-3 ml-3" htmlFor={id}>{label}</label>
      <textarea className="py-3 px-5 rounded-md text-2xl" name={name} id={id} placeholder={placeholder} value={value} onChange={method}></textarea>
    </div>
  )
}

Textarea.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  method: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  id: PropTypes.string
}

export default Textarea
