import { AnimatePresence } from 'framer-motion'
import { useSelector } from 'react-redux'
import Todo from '../Todo'

const Todolist = () => {
  const todolist = useSelector(state => state.todolist)

  return (
    <ul className="grid grid-cols-1 gap-4">
      <AnimatePresence>
        {todolist.map(todo => <Todo key={todo.id} data={todo} />)}
      </AnimatePresence>
    </ul>
  )
}

export default Todolist
