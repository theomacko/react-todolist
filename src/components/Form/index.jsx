import { useState } from "react"
import { useDispatch } from "react-redux"
import { addTodo } from "../../features/todolist/todolistSlice"
import Button from "../Button"
import Checkbox from "../Checkbox"
import Input from "../Input"
import Textarea from "../Textarea"

const Form = () => {
  // Dispatch
  const dispatch = useDispatch()

  // States
  const [error, setError] = useState(false)
  const [title, setTitle] = useState('')
  const [description, setDescription] = useState('')
  const [completed, setCompleted] = useState(false)

  // Methods
  const handleOnTitleChange = (event) => {
    setTitle(event.target.value)
  }

  const handleOnDescriptionChange = (event) => {
    setDescription(event.target.value)
  }

  const handleOnCompletedChange = (event) => {
    setCompleted(!completed)
  }

  const handleOnSubmit = (event) => {
    event.preventDefault()

    if (title.length && description.length) {
      dispatch(addTodo({ title, description, completed }))

      // Reset states
      setError(false)
      setTitle('')
      setDescription('')
      setCompleted(false)
    } else {
      setError(true)
    }
  }

  // Component methods
  const ShowError = () => {
    if (error) {
      return <p className="p-3 mb-5 border-4 rounded-md border-red-900 bg-red-700 text-center">Title and description are required fields</p>
    }
  }

  return (
    <div className="p-10 mb-16 flex flex-col rounded-2xl bg-gray-500">
      <p className="mb-12 text-5xl font-semibold uppercase text-center">add a todo</p>

      <ShowError />

      <form
        className="py-12 px-6 mb-5 grid grid-cols-1 gap-10 border-2 rounded-xl"
        action=""
        method="post"
        onSubmit={handleOnSubmit}
      >
        <Input label="Title" name="title" value={title} method={handleOnTitleChange} />
        <Textarea label="Description" name="description" value={description} method={handleOnDescriptionChange} />
        <Checkbox label="completed" name="completed" checked={completed} method={handleOnCompletedChange} />
        <Button type="submit" title="submit" classes="mt-6 self-center" />
      </form>
    </div>
  )
}

export default Form
