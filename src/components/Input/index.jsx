import PropTypes from 'prop-types'

const Input = ({ label, name, value, method, type = 'text', placeholder = label, id = name }) => {
  return (
    <div className="flex flex-col">
      <label className="mb-3 ml-3" htmlFor={id}>{label}</label>
      <input className="py-3 px-5 rounded-md text-2xl" type={type} name={name} id={id} placeholder={placeholder} value={value} onChange={method} />
    </div>
  )
}

Input.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  method: PropTypes.func.isRequired,
  type: PropTypes.string,
  placeholder: PropTypes.string,
  id: PropTypes.string
}

export default Input
