import { createSlice } from '@reduxjs/toolkit'
import { v4 as uuidv4 } from 'uuid';

export const todolistSlice = createSlice({
  name: 'todolist',
  initialState: [],
  reducers: {
    addTodo: (state, { payload }) => {
      state.push({ id: uuidv4(), ...payload })
    },
    removeTodo: (state, { payload: { id } }) => (
      state.filter(todo => todo.id !== id)
    ),
    toggleCompleteTodo: (state, { payload: { id, completed } }) => {
      state.map(todo => {
        if (todo.id === id) {
          todo.completed = completed
        }

        return todo
      })
    }
  }
})

export const { addTodo, removeTodo, toggleCompleteTodo } = todolistSlice.actions

export default todolistSlice.reducer
